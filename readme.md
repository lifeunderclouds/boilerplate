# Life Under Clouds - Boilerplate Gameserver

This is a work in progress, expect the "Getting started" to not be complete and that you'll need to make some changes or fill in some blanks yourself to get everything up and running.

Let us know if you're missing anything.

## Installation

* After downloading the `gameserver` files, open the whole project in an IDE of your choice.
* *Duplicate* `/server-data/server.cfg.example` and rename the new file to `server.cfg`.
* Add your own steam ID to the `add_principal` (Line 50 at the time of writing).
* Go to https://keymaster.fivem.net and generate a new key (or use one previously made), with this key, update the `sv_licenseKey`'s value.
* Download the [latest windows artifacts](https://runtime.fivem.net/artifacts/fivem/build_server_windows/master/) and unpack the content of the zip into the `/artifacts` folder.

## Starting the server

* With the command prompt, or any other terminal replacement ([Cmder](https://cmder.net/) is a good alternative, with a lot of buildin features), go to the root of the project.
* run `start-server.bat` from the command prompt.
    * *Note:* While you can run the `.bat` file directly from windows, it might intervere with reading back the server output if/when things go wrong. If you don't need the server log after the server closes, feel free to run it directly from windows.
* With the FiveM client, direct connect to `127.0.0.1:30120`.

## Good to know
* Because `sv_master1` is active in the `server.cfg`, the server will not show up in the FiveM server list, this is to prevent random people just joining (happens a lot, even if you say it's whitelist, people try).
* Use `TCP` **AND** `UDP` port forwarding if you want external people to join your boilerplate server.
    * Default port is `30120`, you can change this in the `server.cfg`-file.