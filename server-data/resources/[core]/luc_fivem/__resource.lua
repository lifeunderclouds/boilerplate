resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'

ui_page 'html/scoreboard.html'

files {
  'html/scoreboard.html',
  'html/style.css',
  'html/reset.css',
  'html/listener.js'
}

file 'Newtonsoft.Json.dll'
client_script 'LifeUnderClouds.Core.FiveM.Client.net.dll'
server_script 'LifeUnderClouds.Core.FiveM.Server.net.dll'
