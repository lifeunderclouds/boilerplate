setTick(() => {
  let playerPed = GetPlayerPed(-1);

  showBeta();

  if ( IsPedInAnyVehicle(playerPed, false) ) {
    showSpeed();
  }
});

function showBeta() {
  DrawTxt(`~b~Life Under Clouds ~r~in early but active development.~w~`, 0.77, 0.01, 0.4);
}

function showSpeed () {
  const playerPed = GetPlayerPed(-1);
  const vehicle = GetVehiclePedIsIn(playerPed, false);
  const currentSpeed = GetEntitySpeed(vehicle);

  DrawTxt(`${GetVehicleCurrentGear(vehicle)}`, 0.165, 0.925, 1);
  DrawTxt(`MPH ${Math.round(currentSpeed * 2.236936)}`, 0.18, 0.93, 0.5);
  DrawTxt(`KMH ${Math.round(currentSpeed * 3.6)}`, 0.18, 0.953, 0.5);
}

function DrawTxt (text, x, y, scale = 0.3, right = false) {
  SetTextFont(4);
  SetTextProportional(true);
  SetTextScale(0, scale);
  SetTextDropShadow(1, 0, 0, 0, 255);
  SetTextEdge(1, 0, 0, 0, 255);
  SetTextRightJustify(right);
  SetTextDropShadow();
  SetTextOutline();
  SetTextEntry('STRING');
  AddTextComponentString(text);

  DrawText(x, y);
}
